-------------------------------------------------------------------------------
-- Title      : INTEGER N FREQUENCY DIVIDER
-- Project    : CLOCK GENERATOR AND PHASE CALC 
-------------------------------------------------------------------------------
-- File       : FrequecyDivider.vhd
-- Author     : Jubin MITRA
-- Contact	  : jubin.mitra@cern.ch
-- Company    : VECC, Kolkata, India
-- Created    : 15-01-2015
-- Last update: 15-01-2015
-- Platform   : 
-- Standard   : VHDL'93/08
-------------------------------------------------------------------------------
-- Description: 
-- CLOCK DIVISION FACTOR : DIVISION_FACTOR
-- INPUT  : Reference Clock
-- OUTPUT : Derived   Clock   =  Reference Clock / DIVISION_FACTOR								
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 15-01-2015  1.0      Jubin	Created
-------------------------------------------------------------------------------



--=================================================================================================--
--#################################################################################################--
--=================================================================================================--


library ieee;
use ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all; 
USE ieee.STD_LOGIC_UNSIGNED.all; 
use ieee.numeric_std.all;
use IEEE.math_real.all;

--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--
entity FrequencyDivider is
   generic (
		constant DIVISION_FACTOR    			: integer:= 3
   );
   port (
  
      --================--
      -- Reset & Clocks --
      --================--    
      
      -- Reset:
      ---------
      
      RESET_I                                    : in  std_logic;
  
      -- Input Clock:
      ---------------
      
      CLK_I   		                             : in  std_logic; 
      
      -- Output Divided Clock:
      ------------------------
      
      CLK_O										 : out std_logic	  
   
   );
end entity;

--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--
architecture behavioral of FrequencyDivider is

	--==================================== Constant Definition ====================================--   
	
	constant T_FLIP_FLOP_COUNT					: integer 										 := integer(ceil(log2(real(DIVISION_FACTOR))));
	constant isDivisionFactorEven				: integer 										 := DIVISION_FACTOR mod 2;
	constant DivisionFactorInStdVector			: std_logic_vector(T_FLIP_FLOP_COUNT downto 0)	 := std_logic_vector(to_unsigned(DIVISION_FACTOR,T_FLIP_FLOP_COUNT+1));
	constant DivisionFactorBy2InStdVector		: std_logic_vector(T_FLIP_FLOP_COUNT downto 0)	 := '0' & DivisionFactorInStdVector(T_FLIP_FLOP_COUNT downto 1);
	constant ZeroStdVector						: std_logic_vector(T_FLIP_FLOP_COUNT downto 0)	 := (others=>'0');

	--==================================== Signal Definition ======================================--   
	
	signal CLR									: std_logic										 := 		 '0';
	signal Q									: std_logic_vector(T_FLIP_FLOP_COUNT downto 0)	 := (others=>'0');
	signal Qn									: std_logic_vector(T_FLIP_FLOP_COUNT downto 0)	 := (others=>'0');
	signal T									: std_logic_vector(T_FLIP_FLOP_COUNT downto 0)	 := (others=>'0');

	signal mux_select_lines						: std_logic_vector(1 downto 0)					 := 		"00";
	signal mux_out								: std_logic										 := 		 '0';
   
    --==================================== Component Declaration ==================================--   
	-- Toggle Flip Flop
	-------------------
	component T_FLIP_FLOP is
	   port (    
	   
		CLR_I                                      	: in  std_logic;
		CLK_I   		                            : in  std_logic; 
		T_I											: in  std_logic; 
		Q_O										 	: out std_logic;	  
		Qn_O										: out std_logic	  
	   
	   );
	end component;
   
   --==============================================================================================--  

--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--

   --==================================== User Logic ==============================================--   

   
   --===================================================================================================--
   --================================# N-BIT Synchronous up Counter #===================================--
   --===================================================================================================--
  
	T_gen:
	for i in 0 to T_FLIP_FLOP_COUNT -1 generate
	
		T_FLIP_FLOP_comp: T_FLIP_FLOP
				port map (    
			   
					CLR_I                                      	=> CLR,
					CLK_I   		                            => CLK_I,
					T_I											=>  T(i),
					Q_O										 	=>  Q(i),	  
					Qn_O										=> Qn(i) 
			   
			   );
			   
		T0_signal:   
		if i = 0 generate
					T(0)										<= '1';					
		end generate T0_signal;
		
		T1_signal:   
		if i = 1 generate
					T(1)										<= Q(0);					
		end generate T1_signal;		
		
		Ti_signal:
		if i > 1 generate
					T(i)										<= T(i-1) and Q(i-1);
		end generate Ti_signal;
		
	end generate T_gen;

   --===================================================================================================--
   --================================# Generate Clear signal #==========================================--
   --===================================================================================================--	
	
		CLR					 									<= 	'1' when Q = DivisionFactorInStdVector-1 or RESET_I='1'
															else 	'0';

   --===================================================================================================--
   --===================================# MUX Defination #==============================================--
   --===================================================================================================--		
	-- Mux Circuit is clocked to absorb jitter that arrises due to concurrent transition between states
	-- For odd division factor it need to be clocked on both rising_edge and falling_edge, whereas
	-- for even division factor need only to be clocked on rising_edge to avoid beign 90 deg phase shift
   
	
	mux_out																<= CLK_I when mux_select_lines = "00"
																	else 	'0'	 when mux_select_lines = "01"
																	else 	'1'	 ;
	
   --===================================================================================================--
   --=================================# MUX Line Selection #============================================--
   --===================================================================================================--	

mux_odd_lookup_table:
	if isDivisionFactorEven /= 0 generate
	mux_select_lines											<= 	"00" 	when Q = ZeroStdVector
															else	"01"	when Q > ZeroStdVector and Q <= DivisionFactorBy2InStdVector
															else	"10"	when Q > DivisionFactorBy2InStdVector and Q < DivisionFactorInStdVector
															else	"11";
	end generate mux_odd_lookup_table;

	
mux_even_lookup_table:
	if isDivisionFactorEven = 0 generate
	mux_select_lines											<= 	"01"	when Q >= ZeroStdVector and Q < DivisionFactorBy2InStdVector
															else	"10"	when Q >= DivisionFactorBy2InStdVector and Q < DivisionFactorInStdVector
															else	"11";
	end generate mux_even_lookup_table;
	
   --===================================================================================================--
   --===================================# Derived Clock Output #========================================--
   --===================================================================================================--															
   
   
	CLK_O		<= mux_out;
	
	
end behavioral;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--