-------------------------------------------------------------------------------
-- Title      : INTEGER N FREQUENCY DIVIDER
-- Project    : CLOCK GENERATOR AND PHASE CALC 
-------------------------------------------------------------------------------
-- File       : FrequencyDividerQsysWrapper.vhd
-- Author     : Jubin MITRA
-- Contact	  : jubin.mitra@cern.ch
-- Company    : VECC, Kolkata, India
-- Created    : 15-01-2015
-- Last update: 15-01-2015
-- Platform   : 
-- Standard   : VHDL'93/08
-------------------------------------------------------------------------------
-- Description: 
-- CLOCK DIVISION FACTOR : DIVISION_FACTOR
-- No of CLock links	 : NO_OF_LINKS
-- INPUT  : Reference Clock
-- OUTPUT : Derived   Clock   =  Reference Clock / DIVISION_FACTOR								
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 15-01-2015  1.0      Jubin	Created
-------------------------------------------------------------------------------



--=================================================================================================--
--#################################################################################################--
--=================================================================================================--


library ieee;
use ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all; 
USE ieee.STD_LOGIC_UNSIGNED.all; 
use ieee.numeric_std.all;
use IEEE.math_real.all;

--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--
entity FrequencyDividerQsysWrapper is
   generic (
		constant DIVISION_FACTOR    			: integer := 11;
		constant NUMBER_OF_LINKS				: integer := 6 
   );
   port (
  
      --================--
      -- Reset & Clocks --
      --================--    
      
      -- Reset:
      ---------
      
      RESET_I                                    : in  std_logic;
  
      -- Input Clock:
      ---------------
      
      CLK_I_Link1   		                     : in  std_logic; 
      CLK_I_Link2   		                     : in  std_logic; 
      CLK_I_Link3   		                     : in  std_logic; 
      CLK_I_Link4   		                     : in  std_logic; 
      CLK_I_Link5   		                     : in  std_logic; 
      CLK_I_Link6   		                     : in  std_logic; 
      
      -- Output Divided Clock:
      ------------------------
      
      CLK_O_Link1   		                     : out std_logic; 
      CLK_O_Link2   		                     : out std_logic; 
      CLK_O_Link3   		                     : out std_logic; 
      CLK_O_Link4   		                     : out std_logic; 
      CLK_O_Link5   		                     : out std_logic; 
      CLK_O_Link6   		                     : out std_logic 
   
   );
end entity;

--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--
architecture behavioral of FrequencyDividerQsysWrapper is

	--==================================== Constant Definition ====================================--   
	

	--==================================== Signal Definition ======================================--   
	

   
    --=================================================================================================--
							--========####  Component Declaration  ####========-- 
	--=================================================================================================--   

	component FrequencyDivider is
	   generic (
			constant DIVISION_FACTOR    			: integer:= 3
	   );
	   port (
		  RESET_I                                    : in  std_logic;
		  CLK_I   		                             : in  std_logic; 
		  CLK_O										 : out std_logic	    
	   );
	end component;
   
   --==============================================================================================--  

--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--

   --==================================== User Logic ==============================================--   

	
   --==================================== Port Mapping ======================================--


		FrequencyDivider_comp1:
		   FrequencyDivider
			  generic map(
			  DIVISION_FACTOR							=> DIVISION_FACTOR
			  )
			  port map(
			  RESET_I                                   => RESET_I,
			  CLK_I   		                            => CLK_I_Link1,
			  CLK_O   		                            => CLK_O_Link1
		   );

	FrequencyDivider_gen2:
	if NUMBER_OF_LINKS >= 2 generate
		FrequencyDivider_comp2:
		  FrequencyDivider
			  generic map(
			  DIVISION_FACTOR							=> DIVISION_FACTOR
			  )
			  port map(
			  RESET_I                                   => RESET_I,
			  CLK_I   		                            => CLK_I_Link2,
			  CLK_O   		                            => CLK_O_Link2
		   );		
	end generate FrequencyDivider_gen2;
		   
	FrequencyDivider_gen3:
	if NUMBER_OF_LINKS >= 3 generate
		FrequencyDivider_comp3:
		   FrequencyDivider
			  generic map(
			  DIVISION_FACTOR							=> DIVISION_FACTOR
			  )
			  port map(
			  RESET_I                                   => RESET_I,
			  CLK_I   		                            => CLK_I_Link3,
			  CLK_O   		                            => CLK_O_Link3
		   );		 
	end generate FrequencyDivider_gen3;

	FrequencyDivider_gen4:
	if NUMBER_OF_LINKS >= 4 generate
		FrequencyDivider_comp4:
		   FrequencyDivider
			  generic map(
			  DIVISION_FACTOR							=> DIVISION_FACTOR
			  )
			  port map(
			  RESET_I                                   => RESET_I,
			  CLK_I   		                            => CLK_I_Link4,
			  CLK_O   		                            => CLK_O_Link4
		   );
	end generate FrequencyDivider_gen4;

	FrequencyDivider_gen5:
	if NUMBER_OF_LINKS >= 5 generate
		FrequencyDivider_comp5:
		  FrequencyDivider
			  generic map(
			  DIVISION_FACTOR							=> DIVISION_FACTOR
			  )
			  port map(
			  RESET_I                                   => RESET_I,
			  CLK_I   		                            => CLK_I_Link5,
			  CLK_O   		                            => CLK_O_Link5
		   );		   
	end generate FrequencyDivider_gen5;

	FrequencyDivider_gen6:
	if NUMBER_OF_LINKS >= 6 generate
		FrequencyDivider_comp6:
		   FrequencyDivider
			  generic map(
			  DIVISION_FACTOR							=> DIVISION_FACTOR
			  )
			  port map(
			  RESET_I                                   => RESET_I,
			  CLK_I   		                            => CLK_I_Link6,
			  CLK_O   		                            => CLK_O_Link6
		   );		   
	end generate FrequencyDivider_gen6;

end behavioral;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--