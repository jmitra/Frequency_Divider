vlib work
vcom -work work T_FLIP_FLOP.vhd
vcom -work work FrequencyDivider.vhd
vcom -work work FrequencyDivider_tb.vhd
vsim work.FrequencyDivider_tb
do wave.do
run 1000 ns