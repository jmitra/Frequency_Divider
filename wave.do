onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider {External Signals}
add wave -noupdate /frequencydivider_tb/RESET
add wave -noupdate /frequencydivider_tb/REF_CLK
add wave -noupdate /frequencydivider_tb/OUTPUT_CLK
add wave -noupdate -divider {Internal Signals}
add wave -noupdate -radix unsigned /frequencydivider_tb/FrequencyDivider_comp/DIVISION_FACTOR
add wave -noupdate -radix hexadecimal /frequencydivider_tb/FrequencyDivider_comp/RESET_I
add wave -noupdate -radix hexadecimal /frequencydivider_tb/FrequencyDivider_comp/CLK_I
add wave -noupdate -radix hexadecimal /frequencydivider_tb/FrequencyDivider_comp/CLK_O
add wave -noupdate -radix hexadecimal /frequencydivider_tb/FrequencyDivider_comp/CLR
add wave -noupdate -radix unsigned /frequencydivider_tb/FrequencyDivider_comp/Q
add wave -noupdate -radix unsigned /frequencydivider_tb/FrequencyDivider_comp/Qn
add wave -noupdate -radix unsigned -childformat {{/frequencydivider_tb/FrequencyDivider_comp/T(4) -radix unsigned} {/frequencydivider_tb/FrequencyDivider_comp/T(3) -radix unsigned} {/frequencydivider_tb/FrequencyDivider_comp/T(2) -radix unsigned} {/frequencydivider_tb/FrequencyDivider_comp/T(1) -radix unsigned} {/frequencydivider_tb/FrequencyDivider_comp/T(0) -radix unsigned}} -subitemconfig {/frequencydivider_tb/FrequencyDivider_comp/T(4) {-radix unsigned} /frequencydivider_tb/FrequencyDivider_comp/T(3) {-radix unsigned} /frequencydivider_tb/FrequencyDivider_comp/T(2) {-height 15 -radix unsigned} /frequencydivider_tb/FrequencyDivider_comp/T(1) {-height 15 -radix unsigned} /frequencydivider_tb/FrequencyDivider_comp/T(0) {-height 15 -radix unsigned}} /frequencydivider_tb/FrequencyDivider_comp/T
add wave -noupdate -radix hexadecimal -childformat {{/frequencydivider_tb/FrequencyDivider_comp/mux_select_lines(1) -radix hexadecimal} {/frequencydivider_tb/FrequencyDivider_comp/mux_select_lines(0) -radix hexadecimal}} -subitemconfig {/frequencydivider_tb/FrequencyDivider_comp/mux_select_lines(1) {-height 15 -radix hexadecimal} /frequencydivider_tb/FrequencyDivider_comp/mux_select_lines(0) {-height 15 -radix hexadecimal}} /frequencydivider_tb/FrequencyDivider_comp/mux_select_lines
add wave -noupdate -radix hexadecimal /frequencydivider_tb/FrequencyDivider_comp/mux_out
add wave -noupdate -radix hexadecimal /frequencydivider_tb/FrequencyDivider_comp/T_FLIP_FLOP_COUNT
add wave -noupdate -radix hexadecimal /frequencydivider_tb/FrequencyDivider_comp/isDivisionFactorEven
add wave -noupdate -radix unsigned -childformat {{/frequencydivider_tb/FrequencyDivider_comp/DivisionFactorInStdVector(4) -radix unsigned} {/frequencydivider_tb/FrequencyDivider_comp/DivisionFactorInStdVector(3) -radix unsigned} {/frequencydivider_tb/FrequencyDivider_comp/DivisionFactorInStdVector(2) -radix unsigned} {/frequencydivider_tb/FrequencyDivider_comp/DivisionFactorInStdVector(1) -radix unsigned} {/frequencydivider_tb/FrequencyDivider_comp/DivisionFactorInStdVector(0) -radix unsigned}} -subitemconfig {/frequencydivider_tb/FrequencyDivider_comp/DivisionFactorInStdVector(4) {-radix unsigned} /frequencydivider_tb/FrequencyDivider_comp/DivisionFactorInStdVector(3) {-radix unsigned} /frequencydivider_tb/FrequencyDivider_comp/DivisionFactorInStdVector(2) {-height 15 -radix unsigned} /frequencydivider_tb/FrequencyDivider_comp/DivisionFactorInStdVector(1) {-height 15 -radix unsigned} /frequencydivider_tb/FrequencyDivider_comp/DivisionFactorInStdVector(0) {-height 15 -radix unsigned}} /frequencydivider_tb/FrequencyDivider_comp/DivisionFactorInStdVector
add wave -noupdate -radix unsigned /frequencydivider_tb/FrequencyDivider_comp/DivisionFactorBy2InStdVector
add wave -noupdate -radix hexadecimal /frequencydivider_tb/FrequencyDivider_comp/ZeroStdVector
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {904065 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 186
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {878939 ps} {1006372 ps}
